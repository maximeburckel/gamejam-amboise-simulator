# T4

- Nom du groupe : Dodo (Oiseau aptère de NZ)
- Nom du jeu : Nothing to Lord
- Membres du groupe :
  - BURCKEL Maxime
  - NOEL Arnaud
  - SANZOVO Victor
  - JOLY Dorian
- Lien vers le CDC : [Notre Cahier des Charges](https://docs.google.com/document/d/1WZS6cyi0vQGtonLRVx5YgbKzpWw9VYC_WeliwyO-Sos/edit?usp=sharing)
- Liens vers les évaluations T4 :
  - [Allal Gwénolé](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/evaluations-T4/Evaluation-T4_Gwénolé_Allal.md)
  - [Carlucci Romain](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/evaluations-T4/Evaluation-T4_Romain_Carlucci.md)
  - [Francesconi Léo-Paul](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/evaluations-T4/Evaluation-T4_Léo-Paul_Francesconi.md)
  - [Hasni Yanis](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/evaluations-T4/Evaluation-T4_Yanis_Hasni.md)
  - [Mattina Selina](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/evaluations-T4/Evaluation-T4_Selina_Mattina.md)
  - [Rezki Saber](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/evaluations-T4/Evaluation-T4_Saber_Rezki.md)
  - [Steinmetz Grégoire](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/evaluations-T4/Evaluation-T4_Grégoire_Steinmetz.md)

## Présentation du projet

### Sujet
### Habitants et manants d'Amboise, histoire sociale et économique entre les Amboisiens et leurs rois à la Renaissance jusqu'au début des Guerres de religion (1450-1574)

#### Contexte du scénario
De par son statut particulier et unique, Amboise était une ville remplie d’opportunités pour s’élever socialement et obtenir des seigneuries, voire des titres de noblesse.
Certains achetaient des seigneuries, et, s’ils étaient assez riches, cessent de travailler.
On pouvait aussi essayer de s’attirer les bonnes grâces des nobles et notables de la ville, tel que le maire ou le bailli (représentant de la justice royale), en leur offrant des cadeaux, pécuniaires ou matériels, notamment en échange de titres et de terres. Une stratégie similaire consistait à se marier avec les filles de nobles, dans l’espoir d’hériter de leur titre.

#### Résumé rapide du jeu

On débute la partie en tant que marchand Amboisien du tiers-état. L'objectif du jeu est de devenir noble avant la fin de la partie. Pour cela, il faut commencer à acquérir des titres en rentrant contact avec des nobles ou même un peu moins au début. Vous allez côtoyer des nobles et des individus des autres castes dans votre boutique, leur vendant vos article. C’est grâce à votre commerce que vous allez monter en titre et en grade dans la ville tout en cherchant à faire fortune. C’est un jeu de choix. On peut gagner en réussissant à devenir noble en achetant un titre avec de l’argent et/ou en se mariant avec une fille noble et payant sa dote.

## Procédures d'installation et d'exécution

### Sous Windows :
- Téléchargez [Amboise_Simulator.exe](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/game/Amboise_Simulator.exe)
- Exécutez Amboise_Simulator.exe en double cliquant dessus

### Sous Linux :
- Téléchargez [Amboise_Simulator.sh](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/game/Executable%20Linux/Amboise_Simulator.sh)
- Téléchargez [Amboise_Simulator.x86_64](https://git.unistra.fr/noel-sanzovo/t4/-/blob/master/game/Executable%20Linux/Amboise_Simulator.x86_64) et le mettre dans le même dossier
- Entrez les commandes suivante dans un terminal :
  chmod 777 Amboise_Simulator.x86_64
  ./Amboise_Simulator.x86_64


## Captures d'écran
Ecran de jeu :

![](description_projet/ScreenShots/Game.png)

Ecran de Game Over :
![](description_projet/ScreenShots/Defaite.png)

Ecran de Victoire :
![](description_projet/ScreenShots/Win.png)

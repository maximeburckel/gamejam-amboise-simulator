# Dialogues avec les PNJ (Personnages Non Jouables)
Les PNJ ont des dialogues différents selon leur caste sociale et les offres qu'ils nous proposent.
Leur réponse peut aussi varier selon que l'on accepte ou non la vente.

## Client normal

### Entre dans la boutique

- "Auriez-vous ma taille en stock?"
- "Je ne suis point confortable dans mes sabots. Il m'en faudrait des nouveaux."
- "J'ai besoin d'être mieux vêtu si je veux donner bonne impression!"

### Refus de la vente

- "Mais?! J'ai pourtant l'argent pour payer le prix!"
- "Je m'en souviendrais!"
- "Vous me méprisez, c'est ça?"

### Accepte la vente

- "Merci Bien! Au revoir!"
- "Enfin je trouve chaussure à mon pied!"
- "Voici votre argent, tenez."

## Client fortuné

### Entre dans la boutique
- "Je souhaite avoir ces chausses, et vite! Ne vous en faites pas, je vous paierai le prix! Son double même!"
- "Je souhaite voir vos plus beaux articles! Ne vous en faites pas, j'ai de quoi me les acheter!"

### Refus de la vente

- "L'argent ne vous intéresse donc pas?"
- "Vous n'arriverez jamais à faire fortune!"

### Accepte la vente

- "Merci mon brave! Vous savez reconnaitre les bonnes affaires!"
- "Ces chausses sont des plus exquises! Je vais briller en société!"
 
## Client titré (sage/noble/honorable homme)

### Entre dans la boutique
- "J'aimerais des chaussures me permettant de danser le ballet de cour!"
- "J'ai besoin de chaussures solides, me permettant de pratiquer la chasse à courre!"
- "Manant! Trouvez moi chaussure à mon pied! C'est votre seigneur qui l'ordonne!"
- "Mes chaussures du Dimanche ne ressemblent plus à rien! Que va-t-on dire de moi durant l'office?"

### Refus de la vente

- "Comment osez-vous? Sachez que le Bailli viendra bientôt à votre porte!"
- "Même mon père ne m'a jamais rien refusé!"
- "Vous vous croyez malin, petit moins que rien?"
- "Je vais détruire votre réputation!"

### Accepte la vente

- "Je serais au centre de l'attention durant le prochain bal!"
- "Je pourrais enfin aller par monts et par vaux sans craindre de salir mes semelles! "
- "Merci bien mon brave! Vous valez peut-être plus que les autres manants!"
- "Vous me sauvez! Je parlerais de vous auprès de mes amis haut placés."

## Client ayant un titre de Maître

### Entre dans la boutique

- "Je suis invité à la fête du roi ce soir mais mes Chausses sont boueuses! Je ne peux pas vous payer de suite, mais si vous me sauvez, mon ami, je vous promets de vous rendre la pareil en temps voulu!"

### Refus de la vente

- "Il est vrai que ma méthode de payement est peu conventionnelle, mais je me fourvoie tout de même! Je suis déçu, et attristé. Adieu."

### Accepte la vente

- "Merci mon ami! Vous ne le regretterez pas, je vous rendrais la pareil dans les plus brefs délais!"

### Proposition de mariage

- "Bien le bonjour mon ami! J'ai une bonne nouvelle à t'annoncer! Ma fille unique Cunégonde vient d'atteindre sa majorité, et je souhaite la marier à toi. Oui à toi! Viens fils, dans mes bras!"
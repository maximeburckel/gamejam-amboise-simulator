extends Node2D
var journee
signal timeout

# Called when the node enters the scene tree for the first time.
func _ready():
	var timer = $Timer
	timer.start()
	journee = 1
	$journee_label.text = "Journée "+ str(journee)

func pass_day():
	
	journee = journee +1
	$journee_label.text = "Journée "+ str(journee)
	get_node("../Statistique").recharge_energy()
	get_node("../Statistique").show_impot()
	get_node("../Statistique").decrease_money(int(get_node("../Statistique").money * 0.3))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_timer_timeout():
	get_node("../Statistique").hide_impot()
	pass # Replace with function body.

func get_journee():
	return journee

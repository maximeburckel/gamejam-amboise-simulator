extends Node2D
signal pressOn
var on

# Called when the node enters the scene tree for the first time.
func _ready():
	_setDialogue("Acceptez-vous cette offre ?")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
func _setDialogue(dialogue):
	$dialogue_label.text = dialogue


func _on_o_ui_pressed():
	on = true
	pressOn.emit()
	pass # Replace with function body.


func _on_non_pressed():
	on = false
	pressOn.emit()
	pass # Replace with function body.

func _getOn():
	return on

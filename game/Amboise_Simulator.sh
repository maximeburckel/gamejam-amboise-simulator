#!/bin/sh
echo -ne '\033c\033]0;Amboise_Simulator\a'
base_path="$(dirname "$(realpath "$0")")"
"$base_path/Amboise_Simulator.x86_64" "$@"

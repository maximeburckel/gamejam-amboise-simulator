extends Node2D
var prix
var rand = RandomNumberGenerator.new()
var file_data
var energy
signal win_possible
signal maitre_come
var influence
var image_act = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	rand.randomize()
	_load()
	create_new_classic_character()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func create_new_classic_character():	
	var img_id = rand.randi_range(1,3)
	while img_id == image_act:
		img_id = rand.randi_range(1,3)
	image_act = img_id
	prix = rand.randi_range(5, 20)
	energy = rand.randi_range(2,3)
	influence = rand.randi_range(-3,3)
	var filename = "res://Images/character/classic_character/" + str(img_id) +".png"
	print(filename)
	var image = Image.load_from_file(filename)
	var texture = ImageTexture.create_from_image(image)
	$Texture.texture = texture
	$Texture/Bulle_label.add_theme_font_size_override("font_size", 50)
	$Texture/Bulle_label.text = file_data["classic_character"][str(img_id)]["dialogue"]
	$Texture/prix_label.text = "Prix : +" + str(prix)
	$Texture/energie_label.text = "Energie : -" + str(energy)
	$Texture/influence_label.text = "Influence : " + str(influence)

func _load():
	var file = FileAccess.open("res://Data/character.json", FileAccess.READ)
	var data = JSON.parse_string(file.get_as_text())
	file_data = data
	
func set_energy(e):
	energy = e
func create_new_special_character(influence2,evenement_passe_intr):
	var filename
	var image
	var texture
	var max = 3
	if(influence2<40):
		max = 2
	if(influence2<20):
		max = 1
	var important = rand.randi_range(1,max)
	print("oui" + str(important))
	if(important == 3):
		filename = "res://Images/character/maitre_character/1.png"
		image = Image.load_from_file(filename)
		texture = ImageTexture.create_from_image(image)
		$Texture.texture = texture
		prix = 0
		influence = 50
		$Texture/Bulle_label.add_theme_font_size_override("font_size", 37)
		$Texture/prix_label.text = "Prix : +" + str(prix)
		$Texture/influence_label.text = "Influence : " + str(influence)
		maitre_come.emit()
		if(evenement_passe_intr == false):
			energy = rand.randi_range(4,6)
			$Texture/Bulle_label.text = file_data["maitre_character"][str(1)]["dialogue"]
		else:
			energy = 0
			$Texture/Bulle_label.text = file_data["maitre_character"][str(1)]["evenement"]
			win_possible.emit()
		$Texture/energie_label.text = "Energie : -" + str(energy)
	elif(important == 2):
		var img_id = rand.randi_range(1,4)
		prix = rand.randi_range(50, 100)
		energy = rand.randi_range(4,6)
		influence = rand.randi_range(-5,10)
		filename = "res://Images/character/Important/" + str(img_id) +".png"
		image = Image.load_from_file(filename)
		texture = ImageTexture.create_from_image(image)
		$Texture.texture = texture
		$Texture/Bulle_label.add_theme_font_size_override("font_size", 50)
		$Texture/Bulle_label.text = file_data["titre_character"][str(img_id)]["dialogue"]
		$Texture/prix_label.text = "Prix : +" + str(prix)
		$Texture/energie_label.text = "Energie : -" + str(energy)
		$Texture/influence_label.text = "Influence : " + str(influence)
	else:	#important == 1
		var img_id = rand.randi_range(1,2)
		prix = rand.randi_range(20, 50)
		energy = rand.randi_range(4,6)
		influence = rand.randi_range(-1,5)
		filename = "res://Images/character/fortune_character/" + str(img_id) +".png"
		image = Image.load_from_file(filename)
		texture = ImageTexture.create_from_image(image)
		$Texture.texture = texture
		$Texture/Bulle_label.add_theme_font_size_override("font_size", 50)
		$Texture/Bulle_label.text = file_data["fortune_character"][str(img_id)]["dialogue"]
		$Texture/prix_label.text = "Prix : +" + str(prix)
		$Texture/energie_label.text = "Energie : -" + str(energy)
		$Texture/influence_label.text = "Influence : " + str(influence)

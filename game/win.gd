extends Node2D
var score

# Called when the node enters the scene tree for the first time.
func _ready():
	$background/Label2.text = $background/Label2.text + " " + str(JourneeData.journee) + " journées"
	$background/Label3.text = "Score : " +  Score.score


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_play_pressed():
	get_tree().change_scene_to_file("res://main.tscn")
	pass # Replace with function body.


func _on_quit_pressed():
	get_tree().quit()
	pass # Replace with function body.



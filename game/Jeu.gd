extends Node2D
#La liste de clients 
var nbr_client_before_Spe = 3
var character_passed = 0
var character_passed_max=5
var gameOver
var rng = RandomNumberGenerator.new()
var file_data
var maitre_come = false
var evenement_passe = false
var win_possible = false
signal win

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	gameOver = false
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_statistique_lose():
	get_tree().change_scene_to_file("res://lose.tscn")


func _on_main_character_press_on():
	get_node("Main_Character/OUi").visible = true
	var on = get_node("Main_Character")._getOn()
	if (character_passed == character_passed_max || get_node("Statistique").get_energy() <=0):
		print(str(get_node("client").influence))
		passed_character(on)
		character_passed = 0
		get_node("Journee").pass_day()
		get_node("Main_Character/OUi").visible = true
	else:
		print(str(get_node("client").influence))
		passed_character(on)
		#if(get_node("Statistique").get_energy() - get_node("client").energy <= 0):
			#passed_character(on)
			#character_passed = 0
			#get_node("Journee").pass_day()

func passed_character(on):
	if (on == true):
		print("0.1")
		get_node("Statistique").subtract_energy(get_node("client").energy)
		get_node("Statistique").add_money(get_node("client").prix)
		get_node("Statistique").increase_influence(get_node("client").influence)
		check_pass_day()
		if(maitre_come == true):
			evenement_passe = true
			maitre_come = false
			if(win_possible == true):
				Score.score = str(get_node("Statistique").money * get_node("Statistique").influence)
				JourneeData.journee = str(get_node("Journee").journee)
				get_tree().change_scene_to_file("res://win.tscn")
				win.emit()
	else :
		if(maitre_come == true):
			maitre_come = false
			evenement_passe = false
			win_possible = false
		get_node("client").set_energy(1)
		get_node("Statistique").subtract_energy(get_node("client").energy)
		get_node("Statistique").decrease_money(5)
		check_pass_day()
	if(nbr_client_before_Spe == 0):
		print("0.3")
		nbr_client_before_Spe = rng.randi_range(1,4)
		get_node("client").create_new_special_character(get_node("Statistique").get_influence(), evenement_passe)
	else:
		print("0.4")
		nbr_client_before_Spe = nbr_client_before_Spe -1
		get_node("client").create_new_classic_character()
	if(get_node("client").energy>get_node("Statistique").energy):
		get_node("Main_Character/OUi").visible = false
	character_passed = character_passed+1
	
func check_pass_day():
	if (get_node("Statistique").get_energy() <= 0):
		character_passed = 0
		get_node("Journee").pass_day()

func _on_client_maitre_come():
	maitre_come = true

func _on_client_win_possible():
	win_possible = true
	
func _on_musique_finished():
	$Musique.play(0)
	pass # Replace with function body.

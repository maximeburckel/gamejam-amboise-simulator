extends Node2D

const ENERGYMAX = 10

var energy

var money = 30 

var influence = 10

signal lose

# Called when the node enters the scene tree for the first time.
func _ready():
	energy = ENERGYMAX
	hide_impot()
	$Cadre_stat/money/money_label.text = str(get_money())
	$Cadre_stat/energy/energy_label.text = str(get_energy()) + " / " + str(ENERGYMAX)
	$Cadre_stat/influence/influence_label.text = str(get_influence())
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func subtract_energy(e):
	energy = energy - e
	$Cadre_stat/energy/energy_label.text = str(get_energy()) + " / " + str(ENERGYMAX)

func recharge_energy():
	energy = ENERGYMAX
	$Cadre_stat/energy/energy_label.text = str(get_energy()) + " / " + str(ENERGYMAX)
 
func get_energy():
	return energy

func get_money():
	return money

func get_influence():
	return influence
func add_money(e):
	money = money+e
	$Cadre_stat/money/money_label.text = str(get_money())
func decrease_money(e):
	money = money-e
	$Cadre_stat/money/money_label.text = str(get_money())
	if(money<= 0):
		lose.emit()
func increase_influence(e):
	influence = influence+e
	$Cadre_stat/influence/influence_label.text = str(get_influence())
	if(influence<= 0):
		lose.emit()

func decrease_influence(e):
	influence = influence-e
	$Cadre_stat/influence/influence_label.text = str(get_influence())

func hide_impot():
	$Cadre_stat/Impot.hide()

func show_impot():
	$Cadre_stat/Impot.show()
	
	
